package cr.ac.ucr.ecci.cql.miexamen02;
import android.content.Intent;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;
import cr.ac.ucr.ecci.cql.miexamen02.views.DetailsActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class TestCustomData
{
    private Intent goodIntent = null;
    private Intent emptyIntent = null;
    private Tabletop tabletop = null;

    @Before
    public void initData()
    {
        emptyIntent = new Intent();
        goodIntent = new Intent();
        tabletop = new Tabletop("1", "Cuarentena", 2020, "COVID");

        goodIntent.putExtra("TABLETOP", tabletop);
    }

    @Rule
    // third parameter is set to false which means the activity is not started automatically
    public ActivityTestRule<DetailsActivity> rule =
            new ActivityTestRule<>(DetailsActivity.class, true, false);

    @Test
    public void testCustomDataName() throws InterruptedException
    {
        rule.launchActivity(goodIntent);
        onView(withText("Nombre: Cuarentena")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataID() throws InterruptedException
    {
        rule.launchActivity(goodIntent);
        onView(withText("Identificación: 1")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataPublisher() throws InterruptedException
    {
        rule.launchActivity(goodIntent);
        onView(withText("Editor: COVID")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataYear() throws InterruptedException
    {
        rule.launchActivity(goodIntent);
        onView(withText("Año: 2020")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataNameEmpty() throws InterruptedException
    {
        rule.launchActivity(emptyIntent);
        onView(withText("Nombre: Error")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataIDEmpty() throws InterruptedException
    {
        rule.launchActivity(emptyIntent);
        onView(withText("Identificación: Error")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataPublisherEmpty() throws InterruptedException
    {
        rule.launchActivity(emptyIntent);
        onView(withText("Editor: Error")).check(matches(isDisplayed()));
    }

    @Test
    public void testCustomDataYearEmpty() throws InterruptedException
    {
        rule.launchActivity(emptyIntent);
        onView(withText("Año: 0")).check(matches(isDisplayed()));
    }
}
