package cr.ac.ucr.ecci.cql.miexamen02;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import cr.ac.ucr.ecci.cql.miexamen02.views.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class TestListView
{
    private CountDownLatch lock;

    @Before
    public void initData()
    {
        lock = new CountDownLatch(1);
    }

    @Rule
    public ActivityTestRule<MainActivity> activityRule
            = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testNameMatchesOnClick() throws InterruptedException
    {
        lock.await(5000, TimeUnit.MILLISECONDS);

        onView(withText("Kosmos")).perform(click());

        onView(withText("Nombre: Catan")).check(matches(isDisplayed()));
    }

    @Test
    public void testDialogGetsDismissed() throws InterruptedException
    {
        lock.await(5000, TimeUnit.MILLISECONDS);
        onView(withId(R.string.dialog_text)).check(doesNotExist());
    }
}
