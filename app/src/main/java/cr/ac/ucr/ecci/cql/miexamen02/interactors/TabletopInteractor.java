package cr.ac.ucr.ecci.cql.miexamen02.interactors;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public interface TabletopInteractor
{
    interface OnFinishedListener
    {
        void onFinished(ArrayList<Tabletop> items);
    }

    interface OnProgressListener
    {
        void onProgress(double value);
    }

    void getItems(OnFinishedListener listener, OnProgressListener progressListener);
}
