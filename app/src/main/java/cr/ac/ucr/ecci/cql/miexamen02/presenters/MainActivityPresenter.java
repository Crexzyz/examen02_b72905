package cr.ac.ucr.ecci.cql.miexamen02.presenters;

public interface MainActivityPresenter
{
    void onResume();
    void onDestroy();
    void onItemClicked(int i);
}
