package cr.ac.ucr.ecci.cql.miexamen02.exceptions;

public class CanNotGetItemsException extends Exception
{
    public CanNotGetItemsException(String message)
    {
        super(message);
    }
}
