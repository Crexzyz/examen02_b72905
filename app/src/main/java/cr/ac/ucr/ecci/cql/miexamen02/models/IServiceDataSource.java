package cr.ac.ucr.ecci.cql.miexamen02.models;

import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.exceptions.JsonItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractor;

public interface IServiceDataSource
{
    List<Tabletop> obtainItems(TabletopInteractor.OnProgressListener progressListener) throws JsonItemsException;
}
