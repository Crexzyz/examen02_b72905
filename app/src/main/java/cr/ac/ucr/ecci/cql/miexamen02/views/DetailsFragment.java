package cr.ac.ucr.ecci.cql.miexamen02.views;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

import cr.ac.ucr.ecci.cql.miexamen02.R;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public class DetailsFragment extends Fragment
{
    Tabletop tabletop = null;

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        Resources resources = this.getResources();

        if (getActivity() != null)
        {
            // Colocar nombre
            ((TextView) getActivity().findViewById(R.id.frg_tabletop_name))
                    .setText(String.format(resources.getString(R.string.name_details),
                            this.tabletop.getNombre()));

            // Colocar ID
            ((TextView) getActivity().findViewById(R.id.frg_tabletop_id))
                    .setText(String.format(resources.getString(R.string.id_details),
                            this.tabletop.getIdentificacion()));

            //  Colocar publisher
            ((TextView) getActivity().findViewById(R.id.frg_tabletop_publisher))
                    .setText(String.format(resources.getString(R.string.publisher_details),
                            this.tabletop.getPublisher()));

            // Colocar año
            ((TextView) getActivity().findViewById(R.id.frg_tabletop_year))
                    .setText(String.format(resources.getString(R.string.year_details),
                            this.tabletop.getYear()));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        if(getActivity() != null)
        {
            if(getActivity().getIntent().getExtras() != null)
            {
                this.tabletop = (Tabletop)getActivity().getIntent()
                        .getExtras().getParcelable("TABLETOP");
            }
        }

        if(this.tabletop == null)
            this.tabletop = new Tabletop("Error", "Error", 0, "Error");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }
}