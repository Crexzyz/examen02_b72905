package cr.ac.ucr.ecci.cql.miexamen02.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.R;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public class LazyAdapter extends BaseAdapter
{
    private ArrayList<Tabletop> data;
    private Context context;

    public LazyAdapter(ArrayList<Tabletop> data, Context context)
    {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount()
    {
        return this.data == null ? 0 : this.data.size();
    }

    @Override
    public Object getItem(int i)
    {
        return this.data.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        LayoutInflater inflater =
                (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Tabletop tabletop = this.data.get(i);

        View item = null;

        if(inflater != null)
        {
            item = inflater.inflate(R.layout.item_tabletop, viewGroup, false);

            TextView name = item.findViewById(R.id.item_tabletop_name);
            TextView id = item.findViewById(R.id.item_tabletop_publisher);

            name.setText(tabletop.getNombre());
            id.setText(tabletop.getPublisher());

            return item;
        }

        return null;
    }
}
