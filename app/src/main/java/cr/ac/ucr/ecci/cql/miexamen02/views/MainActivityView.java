package cr.ac.ucr.ecci.cql.miexamen02.views;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public interface MainActivityView
{
    void showProgress();
    void setProgress(double value);
    void hideProgress();
    void setItems(ArrayList<Tabletop> items);
    Tabletop getItem(int i);
    void showMessage(String message);
    void openDetails(int i);
    boolean internetConnectionAvailable();
}

