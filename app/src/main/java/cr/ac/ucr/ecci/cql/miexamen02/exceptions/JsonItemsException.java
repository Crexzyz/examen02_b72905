package cr.ac.ucr.ecci.cql.miexamen02.exceptions;

public class JsonItemsException extends Exception
{
    public JsonItemsException(String message)
    {
        super(message);
    }
}
