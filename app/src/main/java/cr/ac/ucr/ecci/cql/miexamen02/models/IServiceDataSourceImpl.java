package cr.ac.ucr.ecci.cql.miexamen02.models;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.cql.miexamen02.exceptions.JsonItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractor;

public class IServiceDataSourceImpl implements IServiceDataSource
{
    private static final String BITBUCKET_LINK =
            "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop16.json";

    @Override
    public List<Tabletop> obtainItems(TabletopInteractor.OnProgressListener progressListener) throws JsonItemsException
    {
        ArrayList<Tabletop> tabletops = null;

        try
        {
            tabletops = getTabletopFromURL(BITBUCKET_LINK, progressListener);
        }
        catch (Exception e)
        {
            throw new JsonItemsException(e.getMessage());
        }

        return tabletops;
    }

    private ArrayList<Tabletop> getTabletopFromURL(String url, TabletopInteractor.OnProgressListener progressListener)
            throws InterruptedException
    {
        ArrayList<Tabletop> tabletops = new ArrayList<>();
        String json = this.urlToString(url);
        Gson gson = new Gson();

        // Crear objeto Json desde el String de internet
        JsonObject object = gson.fromJson(json, JsonObject.class);
        // Tomar como arreglo los datos de la raíz con nombre 'miTableTop'
        JsonArray array = object.getAsJsonArray("miTabletop");

        if(array != null)
        {
            // Crear cada objeto persona con cada dato del arreglo
            for(int tabletop = 0; tabletop < array.size(); ++tabletop)
            {
                tabletops.add(gson.fromJson(array.get(tabletop), Tabletop.class));
                // Sleep para que se vea el progress
                Thread.sleep(100);
                progressListener.onProgress((double)tabletop/array.size());
            }
        }

        return tabletops;
    }

    private String urlToString(String urlString)
    {
        // Ejemplo tomado de https://stackoverflow.com/questions/7467568/parsing-json-from-url
        BufferedReader reader = null;
        URL url = null;

        try
        {
            url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();

            int read = 0;

            char[] chars = new char[1024];

            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            reader.close();
            return buffer.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return "";
    }
}

