package cr.ac.ucr.ecci.cql.miexamen02.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cr.ac.ucr.ecci.cql.miexamen02.R;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}