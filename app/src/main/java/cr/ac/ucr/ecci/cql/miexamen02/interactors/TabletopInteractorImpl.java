package cr.ac.ucr.ecci.cql.miexamen02.interactors;

import android.os.AsyncTask;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.exceptions.CanNotGetItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;
import cr.ac.ucr.ecci.cql.miexamen02.repositories.TabletopRepository;
import cr.ac.ucr.ecci.cql.miexamen02.repositories.TabletopRepositoryImpl;

public class TabletopInteractorImpl implements TabletopInteractor
{
    TabletopRepository repository;

    @Override
    public void getItems(final OnFinishedListener listener, final OnProgressListener progressListener)
    {
        AsyncTask.execute( new Runnable()
        {
            @Override
            public void run()
            {
                ArrayList<Tabletop> list = null;
                repository = new TabletopRepositoryImpl();

                try
                {
                    list = repository.getItems(progressListener);
                    listener.onFinished(list);
                }
                catch (CanNotGetItemsException e)
                {
                    e.printStackTrace();
                    listener.onFinished(null);
                }
            }
        });
    }
}
