package cr.ac.ucr.ecci.cql.miexamen02.repositories;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.exceptions.CanNotGetItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractor;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public interface TabletopRepository
{
    ArrayList<Tabletop> getItems(TabletopInteractor.OnProgressListener progressListener) throws CanNotGetItemsException;
}
