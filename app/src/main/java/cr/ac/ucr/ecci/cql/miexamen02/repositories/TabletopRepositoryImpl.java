package cr.ac.ucr.ecci.cql.miexamen02.repositories;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.exceptions.CanNotGetItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.exceptions.JsonItemsException;
import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractor;
import cr.ac.ucr.ecci.cql.miexamen02.models.IServiceDataSourceImpl;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;

public class TabletopRepositoryImpl implements TabletopRepository
{
    private cr.ac.ucr.ecci.cql.miexamen02.models.IServiceDataSource IServiceDataSource;

    @Override
    public ArrayList<Tabletop> getItems(TabletopInteractor.OnProgressListener progressListener) throws CanNotGetItemsException
    {
        ArrayList<Tabletop> items = null;

        IServiceDataSource = new IServiceDataSourceImpl();

        try
        {
            items = (ArrayList<Tabletop>) IServiceDataSource.obtainItems(progressListener);
        }
        catch (JsonItemsException e)
        {
            throw new CanNotGetItemsException(e.getMessage());
        }

        return items;
    }
}
