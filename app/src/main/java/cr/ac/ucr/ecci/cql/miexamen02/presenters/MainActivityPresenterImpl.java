package cr.ac.ucr.ecci.cql.miexamen02.presenters;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractor;
import cr.ac.ucr.ecci.cql.miexamen02.interactors.TabletopInteractorImpl;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;
import cr.ac.ucr.ecci.cql.miexamen02.views.MainActivityView;

import static androidx.core.content.ContextCompat.getSystemService;


public class MainActivityPresenterImpl implements MainActivityPresenter,
        TabletopInteractor.OnFinishedListener,
        TabletopInteractor.OnProgressListener
{
    private MainActivityView mainActivityView;
    private TabletopInteractor interactor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView)
    {
        this.mainActivityView = mainActivityView;
        this.interactor = new TabletopInteractorImpl();

        if(mainActivityView.internetConnectionAvailable())
            this.onResume();
        else
            this.mainActivityView.showMessage("No hay conexión a internet, inténtelo más tarde.");
    }

    @Override
    public void onResume()
    {
        if(mainActivityView != null)
            mainActivityView.showProgress();

        interactor.getItems(this, this);
    }

    @Override
    public void onDestroy()
    {
        this.mainActivityView = null;
    }

    @Override
    public void onItemClicked(int i)
    {
        this.mainActivityView.openDetails(i);
    }

    @Override
    public void onFinished(final ArrayList<Tabletop> items)
    {
        if(this.mainActivityView != null)
        {
            ((Activity)mainActivityView).runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    mainActivityView.setItems(items);
                    mainActivityView.hideProgress();
                }
            });

        }
    }

    @Override
    public void onProgress(final double size)
    {
        if(this.mainActivityView != null)
        {
            ((Activity)mainActivityView).runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    mainActivityView.setProgress(size);
                }
            });

        }
    }

}
