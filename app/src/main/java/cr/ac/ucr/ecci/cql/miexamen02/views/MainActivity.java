package cr.ac.ucr.ecci.cql.miexamen02.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import cr.ac.ucr.ecci.cql.miexamen02.R;
import cr.ac.ucr.ecci.cql.miexamen02.adapters.LazyAdapter;
import cr.ac.ucr.ecci.cql.miexamen02.models.Tabletop;
import cr.ac.ucr.ecci.cql.miexamen02.presenters.MainActivityPresenter;
import cr.ac.ucr.ecci.cql.miexamen02.presenters.MainActivityPresenterImpl;

import static androidx.core.content.ContextCompat.getSystemService;

public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener
{
    private ListView listView;
    private MainActivityPresenter presenter;
    private ArrayList<Tabletop> items;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.listView = findViewById(R.id.main_listview);
        this.listView.setOnItemClickListener(this);
        this.presenter = new MainActivityPresenterImpl(this);

    }

    private boolean checkInternetConnection()
    {
        NetworkInfo activeNetworkInfo = null;
        ConnectivityManager connectivityManager = null;

        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager != null)
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void showProgress()
    {
        this.listView.setVisibility(View.INVISIBLE);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setTitle(R.string.dialog_text);
        this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        this.progressDialog.setCancelable(false);
        this.progressDialog.setMax(100);
        this.progressDialog.show();
    }

    @Override
    public void setProgress(double value)
    {
        int progress = (int) Math.round(100 * value);
        this.progressDialog.setProgress(progress);
    }

    @Override
    public void hideProgress()
    {
        this.progressDialog.dismiss();
        this.listView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setItems(ArrayList<Tabletop> items)
    {
        this.items = items;
        LazyAdapter adapter = new LazyAdapter(items, this);
        this.listView.setAdapter(adapter);
    }

    @Override
    public Tabletop getItem(int i)
    {
        return this.items != null ? this.items.get(i) : null;
    }

    @Override
    public void showMessage(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openDetails(int i)
    {
        Tabletop tabletop = this.getItem(i);

        Intent intent =  new Intent(this, DetailsActivity.class);

        if(tabletop != null)
            intent.putExtra("TABLETOP", tabletop);

        this.startActivity(intent);
    }

    @Override
    public boolean internetConnectionAvailable()
    {
        return this.checkInternetConnection();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        presenter.onItemClicked(i);
    }
}